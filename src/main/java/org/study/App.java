package org.study;

import org.json.*;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * Hello world!
 */
public class App {

    private static final String BASE_URI;
    private static final String protocol;
    private static final Optional<String> HOSTNAME;
    private static final String path;
    private static final Optional<String> PORT;
    private static final Random random = new Random();
    private static WebTarget target;

    static {
        protocol = "http://";
        HOSTNAME = Optional.ofNullable(System.getenv("HOSTNAME"));
        PORT = Optional.ofNullable(System.getenv("PORT"));
        path = "listprocessor";
        BASE_URI = protocol + HOSTNAME.orElse("localhost") + ":" + PORT.orElse("7001") + "/" + path;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Start rest client");
        Client c = ClientBuilder.newClient();
        target = c.target(BASE_URI);
        System.out.println("Server address:" + BASE_URI);
        while (true) {
            try {
                if(isServerOk()) {
                    System.out.println("First test. Analise count of entries for each elements in list");
                    testListAnalysis();
                    System.out.println("-----------------------------------------------");
                    System.out.println("Second test.Filter first list by elements of second list");
                    testFilterList();
                    System.out.println("-----------------------------------------------");
                }
            } catch (ProcessingException e) {
                System.out.println("Server not responding. Wait ant try again.");
                Thread.sleep(500);
                continue;
            }
            System.out.println("Press \"Enter\" button to repeat test");
            System.in.read();
        }
    }

    private static void testListAnalysis() {
        List list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(random.nextInt(10) + 1);
        }
        System.out.println("Call service /service/listAnalysis with parameter: list = " + list.toString());
        Response response = target.path("/service/listAnalysis").request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.json(list));
        System.out.println("HttpResponse: " + response.getStatus());
        if(response.getStatus()!=201) {
            System.out.println("Failed!" + response.getStatusInfo());
            return;
        }
        JSONObject jsonObject = new JSONObject(response.readEntity(String.class));
        JSONArray array = jsonObject.getJSONArray("entity");
        System.out.println("Count of duplicates in list: ");
        for (int i = 0; i < array.length(); i++) {
            System.out.println(array.get(i));
        }
    }

    private static void testFilterList() {
        List<Integer> sourceList = new ArrayList<>();
        List<Integer> checkList = new ArrayList<>();
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            sourceList.add(random.nextInt(10) + 1);
        }
        for (int i = 0; i < 5; i++) {
            checkList.add(random.nextInt(10) + 1);
        }

        System.out.println("Call service /service/filterList with parameter:");
        System.out.println("sourceList: " + sourceList.toString());
        System.out.println("checkList: " + checkList.toString());
        List [] lists = new  List[2];
        lists[0]=sourceList;
        lists[1]=checkList;
        Response response = target.path("/service/filterList").request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.json(lists));
        System.out.println("HttpResponse: " + response.getStatus());
        if(response.getStatus()!=201) {
            System.out.println("Failed!");
            return;
        }
        JSONObject jsonObject = new JSONObject(response.readEntity(String.class));
        JSONArray array = jsonObject.getJSONArray("entity");
        System.out.println("Filtered list: ");
        for (int i = 0; i < array.length(); i++) {
            result.add((Integer) array.get(i));
        }
        System.out.println(result);
    }

    private static boolean isServerOk() {
        boolean result;
        System.out.println("-----------------------------------------------");
        System.out.println("Check is server OK");
        System.out.println("Call service /echo");
        Response response = target.path("/service/echo").request().get();
        System.out.println("HttpResponse: " + response.getStatus());
        result = (response.getStatus() == 201);
        System.out.println("Server " + (result ? "OK" : "Not OK"));
        System.out.println("-----------------------------------------------");
        return result;
    }
}
